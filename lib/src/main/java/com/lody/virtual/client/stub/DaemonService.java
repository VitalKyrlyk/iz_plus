package com.lody.virtual.client.stub;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.lody.virtual.R;


/**
 * @author Lody
 *
 */
public class DaemonService extends Service {

    private static final int NOTIFY_ID = 1001;

	public static void startup(Context context) {
		context.startService(new Intent(context, DaemonService.class));
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		startup(this);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
        startService(new Intent(this, InnerService.class));

		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
			NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			String channelId = "com.lody.virtual.client";
			NotificationChannel notificationChannel
					= new NotificationChannel(channelId, channelId, NotificationManager.IMPORTANCE_DEFAULT);
			notificationManager.createNotificationChannel(notificationChannel);
			Notification notification = new Notification.Builder(this, channelId)
					.setContentTitle("IZ Plus 2")
					.setContentText("Connected through SDL")
					.setPriority(Notification.PRIORITY_DEFAULT)
					.build();
			startForeground(NOTIFY_ID, notification);
		} else {
			startForeground(NOTIFY_ID, new Notification());
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return START_STICKY;
	}

	public static final class InnerService extends Service {

        @Override
        public int onStartCommand(Intent intent, int flags, int startId) {
			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
				NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
				String channelId = "com.lody.virtual.client";
				NotificationChannel notificationChannel
						= new NotificationChannel(channelId, channelId, NotificationManager.IMPORTANCE_DEFAULT);
				notificationManager.createNotificationChannel(notificationChannel);
				Notification notification = new Notification.Builder(this, channelId)
						.setContentTitle("IZ Plus 2")
						.setContentText("Connected through SDL")
						.setPriority(Notification.PRIORITY_DEFAULT)
						.build();
				startForeground(NOTIFY_ID, notification);
			} else {
				startForeground(NOTIFY_ID, new Notification());
			}
            stopForeground(true);
            stopSelf();
            return super.onStartCommand(intent, flags, startId);
        }

		@Override
		public IBinder onBind(Intent intent) {
			return null;
		}
	}


}
