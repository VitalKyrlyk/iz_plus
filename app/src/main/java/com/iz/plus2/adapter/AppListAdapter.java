package com.iz.plus2.adapter;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.iz.plus2.R;

import java.util.ArrayList;

/**
 * Created by rapto on 6/16/2017.
 */

public class AppListAdapter extends ArrayAdapter<ApplicationInfo> {
    private final Context context;
    private final ArrayList<ApplicationInfo> apps;

    public AppListAdapter(Context context, ArrayList<ApplicationInfo> values) {
        super(context, R.layout.layout_app_list, values);

        this.context = context;
        this.apps = values;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View rowView = convertView;
        // reuse views
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.layout_app_list, parent, false);
        }

        // fill data
        TextView textView = (TextView) rowView.findViewById(R.id.appLabel);
        ApplicationInfo app = apps.get(position);
        if (app != null)
            textView.setText(context.getPackageManager().getApplicationLabel(app));

        return rowView;
    }
}