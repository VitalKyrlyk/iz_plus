package com.iz.plus2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

import com.iz.plus2.adapter.AppListAdapter;
import com.iz.plus2.service.AppLaunchService;
import com.iz.plus2.service.AppListService;

public class MainActivity extends AppCompatActivity {
    ListView mListView;
    View mTextView, mLoadingView;
    AppCompatActivity self;

    // handle for received Intent for the "launch-app-event" event
    private BroadcastReceiver mAppLaunchListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // hide loading
            mLoadingView.setVisibility(View.INVISIBLE);
        }
    };

    // handler for received Intents for the "list-app-event" event
    private BroadcastReceiver mAppListListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // hide loading
            mLoadingView.setVisibility(View.INVISIBLE);

            // Extract data included in the Intent
            ArrayList<ApplicationInfo> appList = intent.getParcelableArrayListExtra("app-list");

            // Update no data message
            mTextView.setVisibility(appList.isEmpty() ? View.VISIBLE : View.INVISIBLE);

            // update list view
            AppListAdapter adapter = new AppListAdapter(context, appList);
            mListView.setAdapter(adapter);
            mListView.setOnItemClickListener((parent, view, position, id) -> {
                ApplicationInfo app = (ApplicationInfo) parent.getItemAtPosition(position);

                // set preference if first launch
                SharedPreferences setting = getSharedPreferences(VApp.PREF_NAME, 0);
                SharedPreferences.Editor editor = setting.edit();
                editor.putString("pkg", app.packageName);
                editor.apply();

                // show loading
                mLoadingView.setVisibility(View.VISIBLE);

                // start app launching service
                Intent intent1 = new Intent(context, AppLaunchService.class);
                intent1.putExtra("packageName", app.packageName);
                intent1.putExtra("apkPath", app.sourceDir);
                startService(intent1);

                // close or hide me
                self.finish();
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        self = this;

        mListView = (ListView) findViewById(R.id.appListView);
        mTextView = findViewById(R.id.txtEmpty);
        mLoadingView = findViewById(R.id.animLoading);

        // Hide empty message
        mTextView.setVisibility(View.INVISIBLE);

        // show loading
        mLoadingView.setVisibility(View.VISIBLE);

        // start service that get installed app list.
        startService(new Intent(getBaseContext(), AppListService.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mAppListListener, new IntentFilter("list-app-event"));
        LocalBroadcastManager.getInstance(this).registerReceiver(mAppLaunchListener, new IntentFilter("launch-app-event"));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mAppListListener);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mAppLaunchListener);
        super.onPause();
    }
}
