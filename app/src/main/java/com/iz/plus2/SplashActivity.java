package com.iz.plus2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.ipc.VActivityManager;
import com.lody.virtual.remote.InstalledAppInfo;

import org.jdeferred.android.AndroidDeferredManager;

import java.util.List;

public class SplashActivity extends AppCompatActivity {
    boolean isFirst = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        new AndroidDeferredManager().when(() -> {
            long time = System.currentTimeMillis();
            // virtual environment
            VirtualCore.get().waitForEngine();

            // get the last launched app from preference
            String pkg = getSharedPreferences(VApp.PREF_NAME, 0).getString("pkg", null);
            if (pkg != null) {
                // launch "first launched app" if installed
                List<InstalledAppInfo> list = VirtualCore.get().getInstalledApps(0);
                for (InstalledAppInfo app : list) {
                    if (pkg.equals(app.packageName)) {
                        Intent intent1 = VirtualCore.get().getLaunchIntent(app.packageName, 0);
                        VActivityManager.get().startActivity(intent1, 0);
                        isFirst = false;
                        break;
                    }
                }
            }
            time = System.currentTimeMillis() - time;
            long delta = 1000L - time;
            if (delta > 0 && isFirst) {
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).done((res) -> {
            if (isFirst) {
                // go to main activity
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
            }
            finish();
        });
    }

}
