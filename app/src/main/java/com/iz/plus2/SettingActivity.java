package com.iz.plus2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

public class SettingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        Button btnExit = (Button) findViewById(R.id.btnExitSetting);
        btnExit.setOnClickListener((res) -> finish());
    }
}
