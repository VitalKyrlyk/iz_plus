package com.iz.plus2;

import android.app.Application;
import android.content.Context;

import com.lody.virtual.client.core.VirtualCore;
import com.iz.plus2.delegate.MyAppRequestListener;
import com.iz.plus2.delegate.MyComponentDelegate;
import com.iz.plus2.delegate.MyPhoneInfoDelegate;
import com.iz.plus2.delegate.MyTaskDescriptionDelegate;

/**
 * @author Lody
 */
public class VApp extends Application {

    public static String ZALO           = "com.zing.zalo";
    public static String FACEBOOK       = "com.facebook.katana";
    public static String INSTAGRAM      = "com.instagram.android";
    public static String WECHAT         = "com.tencent.mm";
    public static String MOCHA          = "com.viettel.mocha.app";
    public static String OLA            = "chat.ola.vn";
    public static String BEETALK        = "com.beetalk";
    public static String BIGO           = "sg.bigo.live";

    public static String PREF_NAME      = "iz_preference";

    private static VApp gApp;

    public static VApp getApp() {
        return gApp;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        try {
            VirtualCore.get().startup(base);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate() {
        gApp = this;
        super.onCreate();

        VirtualCore virtualCore = VirtualCore.get();
        virtualCore.initialize(new VirtualCore.VirtualInitializer() {
            @Override
            public void onMainProcess() {
            }

            @Override
            public void onVirtualProcess() {
                //listener components
                virtualCore.setComponentDelegate(new MyComponentDelegate());
                //fake phone imei,macAddress,BluetoothAddress
                virtualCore.setPhoneInfoDelegate(new MyPhoneInfoDelegate());
                //fake task description's icon and title
                virtualCore.setTaskDescriptionDelegate(new MyTaskDescriptionDelegate());
            }

            @Override
            public void onServerProcess() {
                virtualCore.setAppRequestListener(new MyAppRequestListener(VApp.this));
            }
        });
    }
}
