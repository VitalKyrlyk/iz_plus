package com.iz.plus2.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

import com.lody.virtual.client.core.InstallStrategy;
import com.lody.virtual.client.core.VirtualCore;
import com.lody.virtual.client.ipc.VActivityManager;

import org.jdeferred.android.AndroidDeferredManager;

public class AppLaunchService extends Service {
    public AppLaunchService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String packageName = intent.getStringExtra("packageName");
        String apkPath = intent.getStringExtra("apkPath");

        new AndroidDeferredManager().when(() -> {
            // install app
            if (!VirtualCore.get().isAppInstalled(packageName))
                VirtualCore.get().installPackage(apkPath, InstallStrategy.COMPARE_VERSION | InstallStrategy.ART_FLY_MODE);
        }).then((res) -> {
            Intent intent1 = VirtualCore.get().getLaunchIntent(packageName, 0);
            VActivityManager.get().startActivity(intent1, 0);
        }).done((res) -> {
            // post result
            Intent resultIntent = new Intent("launch-app-event");
            LocalBroadcastManager.getInstance(this).sendBroadcast(resultIntent);

            // stop me
            stopSelf();
        });

        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
