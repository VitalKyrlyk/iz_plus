package com.iz.plus2.service;

import android.app.Service;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

import com.iz.plus2.VApp;

import org.jdeferred.android.AndroidDeferredManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AppListService extends Service {
    private List<String> pkgs = Arrays.asList(VApp.ZALO, VApp.WECHAT, VApp.FACEBOOK,
            VApp.INSTAGRAM, VApp.MOCHA, VApp.OLA, VApp.BEETALK, VApp.BIGO);

    public AppListService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ArrayList<ApplicationInfo> apps = new ArrayList<>();

        new AndroidDeferredManager().when(() -> {
            // get installed app list
            PackageManager pm = getPackageManager();
            List<ApplicationInfo> totalApps = pm.getInstalledApplications(0);
            for (ApplicationInfo app : totalApps) {
                if (pkgs.contains(app.packageName)) {
                    apps.add(app);
                }
            }
        }).done((res) -> {
            // post result
            Intent resultIntent = new Intent("list-app-event");
            resultIntent.putParcelableArrayListExtra("app-list", apps);
            LocalBroadcastManager.getInstance(this).sendBroadcast(resultIntent);

            // stop me
            stopSelf();
        });

        return START_NOT_STICKY;
    }

}
